import React, {useEffect, useRef} from 'react';
import styles from './style.module.scss'

function App() {
  const servicesRef = useRef<any>(null)
  const stickyRef = useRef<any>(null)

  useEffect(() => {
    window.addEventListener('scroll', () => {
      console.log(servicesRef.current?.clientHeight)
      console.log(stickyRef.current?.getBoundingClientRect());
    })
  }, [servicesRef, stickyRef])

  return (
    <div className={styles.app}>
      <div className={styles.section}></div>
      <div className={styles.services} ref={servicesRef}>
        <div className={styles.services_left}>
          <div className={styles.sticky} ref={stickyRef}>
            <div className={styles.circle}>
              <svg
                version="1.1"
                id="star-svg"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                x="0px"
                y="0px"
                viewBox="0 0 600 600"
                xmlSpace="preserve">
                <circle
                  id="star-path"
                  fill="none"
                  stroke="#000000"
                  strokeWidth="10"
                  cx="300"
                  cy="300"
                  r="290"
                  style={{
                    strokeDasharray: '1821.39, 1821.39',
                    // strokeDashoffset: '1457.49',
                  }}
                ></circle>
              </svg>
            </div>
            <div className={styles.line}></div>
          </div>
        </div>
        <div className={styles.services_right}>
          <div className={styles.collapse}></div>
          <div className={styles.collapse}></div>
          <div className={styles.collapse}></div>
          <div className={styles.collapse}></div>
          <div className={styles.collapse}></div>
          <div className={styles.collapse}></div>
          <div className={styles.collapse}></div>
          <div className={styles.collapse}></div>
          <div className={styles.collapse}></div>
          <div className={styles.collapse}></div>
        </div>
      </div>
      <div className={styles.section}></div>
    </div>
  );
}

export default App;
